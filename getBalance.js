const rpcUrl = 'http://rpc-bsc-main-01.3tokendigital.com/rpc'
const walletAddress = '0x32D8A4BEA74e0930AD0Be04c5E9DB31Ad33A11fD';

const Web3 = require('web3');
const provider = new Web3.providers.HttpProvider(rpcUrl);
const web3 = new Web3(provider);
const balance = web3.eth.getBalance(walletAddress);

balance
    .then(data => console.log(data))
    .catch(err => console.error(err))
